class Main
  
  # Esse é o main da classe
  def initialize
    teste_errado
    teste_certo
  end
  
  def teste_errado
    #Abrindo os arquivos pra escrita
    teste = File.new(File.expand_path(File.dirname(__FILE__) + "/TesteErrado.txt"), "w")
    saida = File.new(File.expand_path(File.dirname(__FILE__) + "/SaidaDoErrado.txt"), "w")

    #Criando um "arranjo" de 1 ate 10, ele pega cada valor joga em i e faz o loop
    #se quiser mais testes errados aumente aqui :)
    (1..100).each do |i|
      #metodo rand sorteia um numero de 0 ate i
      x = rand(i)
      y = rand(i*10)
      z = rand(i*5)

      #O metodo to_s(2) transforma o numero inteiro em uma string que representa o numero binario correspondente
      #Em ruby você pode executar codigo dentro de string ou apresentar variaveis, basta jogar dentro de #{aqui_vai_o_que_ce_quer}
      binary_num = "$#{x.to_s(2)}*#{y.to_s(2)}=#{z.to_s(2)}\n"
      #verificando se a operação é verdareira ou falsa
      real_num = x * y == z
      
      #gravando a string que contem os numeros binarios
      teste.syswrite "#{binary_num}"
      #gravando o resultado esperado dessa operacao
      saida.syswrite "#{real_num}\n"
    end

    teste.close
    saida.close
  end
  
  def teste_certo
    #abrindo os arquivos para escrita
    teste = File.new(File.expand_path(File.dirname(__FILE__) + "/TesteCerto.txt"), "w")
    saida = File.new(File.expand_path(File.dirname(__FILE__) + "/SaidaDoCerto.txt"), "w")
    
    #Se quiser aumentar o numero de testes certos aumente aqui 
    (1..100).each do |i|
      #criando numeros randomicos
      x = rand(i)
      y = rand(i*10)
      z = x * y #garantindo que a operacao vai ser sempre verdadeira
      
      #gravando a string com os numeros em binario
      binary_num = "$#{x.to_s(2)}*#{y.to_s(2)}=#{z.to_s(2)}\n"
      num = "#{x}*#{y}=#{z}\n" #gravando a operacao em numeros decimais so pra ter certeza
      #escrevendo a porra toda no arquivo
      teste.syswrite "#{binary_num}"
      saida.syswrite"#{num}"
    end

    teste.close
    saida.close
  end
  
end
#Cria o objeto a partir do metodo initialize
Main.new